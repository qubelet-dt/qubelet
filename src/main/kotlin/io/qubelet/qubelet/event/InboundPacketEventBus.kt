package io.qubelet.qubelet.event

import io.qubelet.qubelet.prot.InboundPacket

object InboundPacketEventBus : EventBus<InboundPacket.Packet>()