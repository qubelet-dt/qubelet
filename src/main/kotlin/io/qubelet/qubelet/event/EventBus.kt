package io.qubelet.qubelet.event

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

open class EventBus<E : Event> {
    val pre: BroadcastChannel<E> = ConflatedBroadcastChannel<E>()
    val post: BroadcastChannel<E> = ConflatedBroadcastChannel<E>()

    fun handlePre(o: E) {
        runBlocking {
            pre.send(o)
        }
    }

    fun handlePost(o: E) {
        runBlocking {
            post.send(o)
        }
    }

    inline fun <reified T> subscribePre(crossinline handler: (T) -> Unit) {
        GlobalScope.launch {
            pre.openSubscription().consumeAsFlow().filter { it is T }.map { it as T }.collect { obj: T -> handler(obj) }
        }
    }

    inline fun <reified T> subscribePost(crossinline handler: (T) -> Unit) {
        GlobalScope.launch {
            post.openSubscription().consumeAsFlow().filter { it is T }.map { it as T }.collect { obj: T -> handler(obj) }
        }
    }
}