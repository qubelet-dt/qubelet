package io.qubelet.qubelet.prot.datatypes

enum class Dimension(val id: Int) {
    NETHER(-1),
    OVERWORLD(0),
    END(1)
}