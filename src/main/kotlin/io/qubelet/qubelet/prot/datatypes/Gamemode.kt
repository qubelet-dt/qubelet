package io.qubelet.qubelet.prot.datatypes

enum class Gamemode(var id: Int) {
    SURVIVAL(0),
    CREATIVE(1),
    ADVENTURE(2),
    SPECTATOR(3);

    fun setHardcore(hardcore: Boolean) {
        if (hardcore) {
            id = id or 0x8
        }
    }
}