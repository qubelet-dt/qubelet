package io.qubelet.qubelet.prot.datatypes

enum class LevelType(val id: String) {
    DEFAULT("default"),
    FLAT("flat"),
    LARGE_BIOMES("largeBiomes"),
    AMPLIFIED("amplified"),
    CUSTOMIZED("customized"),
    BUFFET("buffet"),
    DEFAULT_1_1("default_1_1")
}