package io.qubelet.qubelet.prot

import io.qubelet.qubelet.net.GameConnection
import io.qubelet.qubelet.prot.inbound.handshake.Handshake
import io.qubelet.qubelet.prot.inbound.login.EncryptionResponse
import io.qubelet.qubelet.prot.inbound.login.LoginStart
import io.qubelet.qubelet.prot.inbound.status.Ping
import io.qubelet.qubelet.prot.inbound.status.Request

/**
 * This object contains every single inbound packet, its used to find which packet decode function should be called.
 *
 * Note: This should be changed to an enum class, or even better a way to register the packet from the packet class
 *
 * @property incoming The map containing all the inbound packets
 */
object GamePackets {
    val incoming: Map<GameConnection.State, List<InboundPacket>> = mapOf(
            GameConnection.State.HANDSHAKE to listOf(Handshake),
            GameConnection.State.STATUS to listOf(Request, Ping),
            GameConnection.State.LOGIN to listOf(LoginStart, EncryptionResponse),
            GameConnection.State.PLAY to listOf()
    )
}