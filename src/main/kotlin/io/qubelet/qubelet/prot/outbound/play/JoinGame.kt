package io.qubelet.qubelet.prot.outbound.play

import com.github.simplenet.packet.Packet
import io.qubelet.qubelet.Qubelet
import io.qubelet.qubelet.net.GameConnection
import io.qubelet.qubelet.net.putMCString
import io.qubelet.qubelet.net.putVarInt
import io.qubelet.qubelet.prot.OutboundPacket
import io.qubelet.qubelet.prot.datatypes.Dimension
import io.qubelet.qubelet.prot.datatypes.LevelType
import java.nio.ByteBuffer
import java.security.MessageDigest

data class JoinGame(override val connection: GameConnection, override val payload: Payload) : OutboundPacket {
    override val opcode = 0x01

    override val packet: Packet

    init {
        val worldSeed = ByteBuffer.allocate(8).putLong(payload.worldSeed).array()
        val digest = MessageDigest.getInstance("SHA-256")
        val worldSeedHash = digest.digest(worldSeed)
        val worldSeedFinal = ByteBuffer.allocate(worldSeedHash.size).long

        packet = Packet.builder()
                .putVarInt(opcode)
                .putInt(connection.player.id)
                .putByte(connection.player.gamemode.id)
                .putInt(connection.player.dimension.id)
                .putLong(worldSeedFinal)
                .putByte(Qubelet.MAX_PLAYERS)
                .putMCString(payload.levelType.id)
                .putVarInt(connection.player.viewDistance)
                .putBoolean(true)
                .putBoolean(false) // TODO: Set this to an incoming parameter, hardcoding for now.
    }

    data class Payload(
            val worldSeed: Long,
            val levelType: LevelType
    ) : OutboundPacket.Payload
}