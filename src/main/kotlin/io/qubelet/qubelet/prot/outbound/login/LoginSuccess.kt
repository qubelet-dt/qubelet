package io.qubelet.qubelet.prot.outbound.login

import com.github.simplenet.packet.Packet
import io.qubelet.qubelet.net.GameConnection
import io.qubelet.qubelet.net.putMCString
import io.qubelet.qubelet.net.putVarInt
import io.qubelet.qubelet.prot.OutboundPacket

data class LoginSuccess(override val connection: GameConnection, override val payload: Payload) : OutboundPacket {
    override val opcode = 0x02
    override val packet: Packet

    init {
        packet = Packet.builder()
                .putVarInt(opcode)
                .putMCString(payload.uuid)
                .putMCString(payload.username)
    }

    data class Payload(
            val uuid: String,
            val username: String
    ) : OutboundPacket.Payload
}