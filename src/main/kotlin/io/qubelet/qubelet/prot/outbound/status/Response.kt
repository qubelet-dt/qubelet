package io.qubelet.qubelet.prot.outbound.status

import com.github.simplenet.packet.Packet
import com.google.gson.JsonObject
import io.qubelet.qubelet.net.GameConnection
import io.qubelet.qubelet.net.putMCString
import io.qubelet.qubelet.net.putVarInt
import io.qubelet.qubelet.prot.OutboundPacket

data class Response(override val connection: GameConnection, override val payload: Payload) : OutboundPacket {
    override val opcode = 0x00
    override val packet: Packet

    init {
        packet = Packet.builder()
                .putVarInt(opcode)
                .putMCString(payload.jsonResponse.toString())
    }

    data class Payload(
            val jsonResponse: JsonObject
    ) : OutboundPacket.Payload
}
