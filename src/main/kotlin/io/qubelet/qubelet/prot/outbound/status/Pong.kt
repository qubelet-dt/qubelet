package io.qubelet.qubelet.prot.outbound.status

import com.github.simplenet.packet.Packet
import io.qubelet.qubelet.net.GameConnection
import io.qubelet.qubelet.net.putVarInt
import io.qubelet.qubelet.prot.OutboundPacket

data class Pong(override val connection: GameConnection, override val payload: Payload) : OutboundPacket {
    override val opcode = 0x01

    override val packet: Packet

    init {
        packet = Packet.builder()
                .putVarInt(opcode)
                .putLong(payload.payload)
    }

    data class Payload(
            val payload: Long
    ) : OutboundPacket.Payload
}