package io.qubelet.qubelet.prot.outbound.login

import com.github.simplenet.packet.Packet
import io.qubelet.qubelet.net.GameConnection
import io.qubelet.qubelet.net.putMCString
import io.qubelet.qubelet.net.putVarInt
import io.qubelet.qubelet.prot.OutboundPacket
import java.security.PublicKey

data class EncryptionRequest(override val connection: GameConnection, override val payload: Payload) : OutboundPacket {
    override val opcode = 0x01
    override val packet: Packet

    init {
        packet = Packet.builder()
                .putVarInt(opcode)
                .putMCString("")
                .putVarInt(payload.publicKey.encoded.size)
                .putBytes(*payload.publicKey.encoded)
                .putVarInt(payload.verifyToken.size)
                .putBytes(*payload.verifyToken)
    }

    data class Payload(
            val publicKey: PublicKey,
            val verifyToken: ByteArray
    ) : OutboundPacket.Payload {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Payload

            if (publicKey != other.publicKey) return false
            if (!verifyToken.contentEquals(other.verifyToken)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = publicKey.hashCode()
            result = 31 * result + verifyToken.contentHashCode()
            return result
        }
    }
}