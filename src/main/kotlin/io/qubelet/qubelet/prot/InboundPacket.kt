package io.qubelet.qubelet.prot

import io.qubelet.qubelet.event.Event
import io.qubelet.qubelet.net.GameConnection
import java.nio.ByteBuffer

/**
 * The interface all inbound packets extend from
 *
 * @property opcode The opcode of the packet
 */
interface InboundPacket {
    val opcode: Int

    /**
     * The function that decodes the data, this will be completely overridden in all packets.
     */
    fun decode(bytes: ByteBuffer, connection: GameConnection): Packet

    /**
     * The data class that holds the refined data of this packet
     */
    interface Packet : Event {
        val connection: GameConnection
    }
}