package io.qubelet.qubelet.prot

import com.github.simplenet.packet.Packet
import io.qubelet.qubelet.event.Event
import io.qubelet.qubelet.net.GameConnection

/**
 * The interface all outbound packets extend from
 *
 * @property opcode The opcode of the packet
 * @property connection The connection of the client this will be sent to
 * @property packet The actual packet containing the data
 * @property packet This payload will be adapted to each packet, its the data in its refined state that the packet will encode
 */
interface OutboundPacket : Event {
    val opcode: Int
    val connection: GameConnection
    val packet: Packet
    val payload: Payload

    /**
     * A blank interface that all payloads extends from
     */
    interface Payload
}
