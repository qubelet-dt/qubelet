package io.qubelet.qubelet.prot.inbound.login

import io.qubelet.qubelet.net.GameConnection
import io.qubelet.qubelet.net.string
import io.qubelet.qubelet.prot.InboundPacket
import java.nio.ByteBuffer

object LoginStart : InboundPacket {
    override val opcode = 0x00

    override fun decode(bytes: ByteBuffer, connection: GameConnection): LoginStartPacket {
        return LoginStartPacket(
                connection,
                bytes.string
        )
    }

    data class LoginStartPacket(
            override val connection: GameConnection,
            val username: String
    ) : InboundPacket.Packet
}