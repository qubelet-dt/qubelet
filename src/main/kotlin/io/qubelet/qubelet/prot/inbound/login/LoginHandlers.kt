package io.qubelet.qubelet.prot.inbound.login

import com.google.gson.Gson
import com.google.gson.JsonObject
import io.qubelet.qubelet.event.InboundPacketEventBus
import io.qubelet.qubelet.net.crypto.AESCipher
import io.qubelet.qubelet.net.crypto.RSAKeyPair
import io.qubelet.qubelet.net.sendPacket
import io.qubelet.qubelet.prot.outbound.login.EncryptionRequest
import io.qubelet.qubelet.prot.outbound.login.LoginSuccess
import java.math.BigInteger
import java.net.URL
import java.security.MessageDigest
import java.security.SecureRandom


object LoginHandlers {
    init {
        InboundPacketEventBus.subscribePost<LoginStart.LoginStartPacket> { onLoginStart(it) }
        InboundPacketEventBus.subscribePost<EncryptionResponse.EncryptionResponsePacket> { onEncryptionResponse(it) }
    }

    private fun onLoginStart(packet: LoginStart.LoginStartPacket) {
        packet.connection.verifyToken = SecureRandom.getSeed(4)
        packet.connection.player.username = packet.username

        val encryptionRequest = EncryptionRequest.Payload(
                RSAKeyPair.keyPair.public,
                packet.connection.verifyToken
        )

        packet.connection.client.sendPacket(EncryptionRequest(packet.connection, encryptionRequest))
    }

    private fun onEncryptionResponse(packet: EncryptionResponse.EncryptionResponsePacket) {
        val sharedSecret = RSAKeyPair.decryption.doFinal(packet.sharedSecret)
        val verifyToken = RSAKeyPair.decryption.doFinal(packet.verifyToken)

        if (verifyToken!!.contentEquals(packet.connection.verifyToken)) {
            val aesCipher = AESCipher(sharedSecret)
            packet.connection.client.decryptionCipher = aesCipher.decryption
            packet.connection.client.encryptionCipher = aesCipher.encryption

            val digest = MessageDigest.getInstance("SHA-1")
            digest.update("".toByteArray()) // Server ID, is always null
            digest.update(sharedSecret)
            digest.update(RSAKeyPair.keyPair.public.encoded)
            val clientHash = BigInteger(digest.digest()).toString(16)

            val playerInfoRaw = URL("https://sessionserver.mojang.com/session/minecraft/hasJoined?username=${packet.connection.player.username}" +
                    "&serverId=${clientHash}").readText()
            val playerInfo = Gson().fromJson(playerInfoRaw, JsonObject::class.java)

            val id = playerInfo["id"].asString.replace(
                    "(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})".toRegex(),
                    "$1-$2-$3-$4-$5"
            )

            val loginSuccess = LoginSuccess.Payload(
                    id,
                    packet.connection.player.username
            )

            packet.connection.client.sendPacket(LoginSuccess(packet.connection, loginSuccess))
        }
    }
}