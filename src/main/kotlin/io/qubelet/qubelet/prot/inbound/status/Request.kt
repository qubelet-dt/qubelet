package io.qubelet.qubelet.prot.inbound.status

import io.qubelet.qubelet.net.GameConnection
import io.qubelet.qubelet.prot.InboundPacket
import java.nio.ByteBuffer

object Request : InboundPacket {
    override val opcode = 0x00

    override fun decode(bytes: ByteBuffer, connection: GameConnection): RequestPacket {
        return RequestPacket(
                connection
        )
    }

    data class RequestPacket(
            override val connection: GameConnection
    ) : InboundPacket.Packet
}