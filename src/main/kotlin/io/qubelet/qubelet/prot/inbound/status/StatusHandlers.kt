package io.qubelet.qubelet.prot.inbound.status

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.qubelet.qubelet.Qubelet
import io.qubelet.qubelet.event.InboundPacketEventBus
import io.qubelet.qubelet.net.sendPacket
import io.qubelet.qubelet.prot.outbound.status.Pong
import io.qubelet.qubelet.prot.outbound.status.Response

object StatusHandlers {
    init {
        InboundPacketEventBus.subscribePost<Request.RequestPacket> { onRequest(it) }
        InboundPacketEventBus.subscribePost<Ping.PingPacket> { onPing(it) }

    }

    private fun onRequest(packet: Request.RequestPacket) {
        val jsonResponse = JsonObject()

        val version = JsonObject()
        version.addProperty("name", Qubelet.VERSION)
        version.addProperty("protocol", Qubelet.PROTOCOL)
        jsonResponse.add("version", version)

        val players = JsonObject()
        players.addProperty("max", Qubelet.MAX_PLAYERS)
        players.addProperty("online", Qubelet.ONLINE_PLAYERS)
        players.add("sample", JsonArray())
        jsonResponse.add("players", players)

        val description = JsonObject()
        description.addProperty("text", Qubelet.DESCRIPTION)
        jsonResponse.add("description", description)

        val response: Response = Response(packet.connection, Response.Payload(
                jsonResponse
        ))

        packet.connection.client.sendPacket(response)
    }

    private fun onPing(packet: Ping.PingPacket) {
        val pong = Pong(packet.connection, Pong.Payload(packet.payload))
        packet.connection.client.sendPacket(pong)
    }
}