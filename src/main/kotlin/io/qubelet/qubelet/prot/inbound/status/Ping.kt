package io.qubelet.qubelet.prot.inbound.status

import io.qubelet.qubelet.net.GameConnection
import io.qubelet.qubelet.prot.InboundPacket
import java.nio.ByteBuffer

object Ping : InboundPacket {
    override val opcode = 0x01

    override fun decode(bytes: ByteBuffer, connection: GameConnection): PingPacket {
        return PingPacket(
                connection,
                bytes.long
        )
    }

    data class PingPacket(
            override val connection: GameConnection,
            val payload: Long
    ) : InboundPacket.Packet
}