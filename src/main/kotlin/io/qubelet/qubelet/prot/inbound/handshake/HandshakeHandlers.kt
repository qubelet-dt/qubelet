package io.qubelet.qubelet.prot.inbound.handshake

import io.qubelet.qubelet.event.InboundPacketEventBus

object HandshakeHandlers {
    init {
        InboundPacketEventBus.subscribePre { packet: Handshake.HandshakePacket ->
            packet.connection.state = packet.nextState
        }
    }
}