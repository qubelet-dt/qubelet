package io.qubelet.qubelet.prot.inbound.login

import io.qubelet.qubelet.net.GameConnection
import io.qubelet.qubelet.net.varInt
import io.qubelet.qubelet.prot.InboundPacket
import java.nio.ByteBuffer

object EncryptionResponse : InboundPacket {
    override val opcode = 0x01

    override fun decode(bytes: ByteBuffer, connection: GameConnection): EncryptionResponsePacket {
        val sharedSecret = ByteArray(bytes.varInt)
        bytes.get(sharedSecret)
        val verifyToken = ByteArray(bytes.varInt)
        bytes.get(verifyToken)

        return EncryptionResponsePacket(
                connection,
                sharedSecret,
                verifyToken
        )
    }

    data class EncryptionResponsePacket(
            override val connection: GameConnection,
            val sharedSecret: ByteArray,
            val verifyToken: ByteArray
    ) : InboundPacket.Packet {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as EncryptionResponsePacket

            if (connection != other.connection) return false
            if (!sharedSecret.contentEquals(other.sharedSecret)) return false
            if (!verifyToken.contentEquals(other.verifyToken)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = connection.hashCode()
            result = 31 * result + sharedSecret.contentHashCode()
            result = 31 * result + verifyToken.contentHashCode()
            return result
        }
    }
}