package io.qubelet.qubelet.prot.inbound.play

import io.qubelet.qubelet.event.OutboundPacketEventBus
import io.qubelet.qubelet.net.sendPacket
import io.qubelet.qubelet.prot.datatypes.LevelType
import io.qubelet.qubelet.prot.outbound.login.LoginSuccess
import io.qubelet.qubelet.prot.outbound.play.JoinGame
import io.qubelet.qubelet.world.World

object PlayHandlers {
    init {
        OutboundPacketEventBus.subscribePost<LoginSuccess> { onLoginSuccess(it) }
    }

    private fun onLoginSuccess(packet: LoginSuccess) {
        val joinGame = JoinGame.Payload(
                World.seed,
                LevelType.DEFAULT
        )

        packet.connection.client.sendPacket(JoinGame(packet.connection, joinGame))
    }
}