package io.qubelet.qubelet.prot.inbound.handshake

import io.qubelet.qubelet.net.GameConnection
import io.qubelet.qubelet.net.string
import io.qubelet.qubelet.net.varInt
import io.qubelet.qubelet.prot.InboundPacket
import java.nio.ByteBuffer

object Handshake : InboundPacket {
    override val opcode: Int = 0x00

    override fun decode(bytes: ByteBuffer, connection: GameConnection): HandshakePacket {
        return HandshakePacket(
                connection,
                bytes.varInt,
                bytes.string,
                bytes.short,
                GameConnection.State.values()[bytes.varInt]
        )
    }

    data class HandshakePacket(
            override val connection: GameConnection,
            val protocolVersion: Int,
            val serverAddress: String,
            val serverPort: Short,
            val nextState: GameConnection.State
    ) : InboundPacket.Packet
}