package io.qubelet.qubelet.net

import com.github.simplenet.Client
import com.github.simplenet.packet.Packet
import io.qubelet.qubelet.event.InboundPacketEventBus
import io.qubelet.qubelet.event.OutboundPacketEventBus
import io.qubelet.qubelet.prot.OutboundPacket
import org.slf4j.LoggerFactory
import java.nio.ByteBuffer
import java.nio.charset.Charset
import kotlin.experimental.and
import kotlin.experimental.or

fun Client.readVarIntAlways(callback: (Int) -> Unit) {
    var i = 0
    var result = 0

    readByteAlways {
        val value = it and 0x7f
        result = result or (value.toInt() shl (7 * i++))

        if (i > 5) {
            throw RuntimeException("VarInt was too big")
        }

        if ((it.toInt() and 0x80) == 0) {
            callback(result)
            result = 0
            i = 0
        }
    }
}

private fun ByteBuffer.varNum(maxBytes: Int): Long {
    var numRead = 0
    var result: Long = 0
    var read: Long
    do {
        read = get().toLong()
        val value: Long = read and 127
        result = result or (value shl 7 * numRead)
        numRead++
        if (numRead > maxBytes) {
            throw RuntimeException("VarNum is too big")
        }
    } while ((read and 128) != 0.toLong())

    return result
}

val ByteBuffer.varInt: Int
    get() = varNum(5).toInt()

val ByteBuffer.varLong: Long
    get() = varNum(10)

val ByteBuffer.string: String
    get() {
        val size = varInt
        val bytes = ByteArray(size)
        get(bytes)
        return bytes.toString(Charset.defaultCharset())
    }

val Client.connection: GameConnection
    get() = ConnHandler.connections[this]!!

fun Packet.putVarInt(data: Int): Packet {
    var value = data
    do {
        var temp = (value and 127).toByte()
        value = value shr 7
        if (value != 0) {
            temp = temp or 128.toByte()
        }
        putByte(temp.toInt())
    } while (value != 0)
    return this
}

fun Packet.putMCString(data: String): Packet {
    putVarInt(data.toByteArray().size)
    putBytes(*data.toByteArray())

    return this
}

val logger = LoggerFactory.getLogger(Client::class.java)

fun Client.sendPacket(packet: OutboundPacket) {
    logger.debug(packet.toString())

    OutboundPacketEventBus.handlePre(packet)

    packet.packet.prepend { it.putVarInt(it.size) }

    packet.packet.queueAndFlush(packet.connection.client)

    OutboundPacketEventBus.handlePost(packet)
}
