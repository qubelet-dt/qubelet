package io.qubelet.qubelet.net

import com.github.simplenet.Client
import io.qubelet.qubelet.entity.Player

class GameConnection {
    enum class State {
        HANDSHAKE,
        STATUS,
        LOGIN,
        PLAY
    }

    var state: State = State.HANDSHAKE
    var player = Player()
    lateinit var client: Client
    lateinit var verifyToken: ByteArray
}