package io.qubelet.qubelet.net

import com.github.simplenet.Client
import io.qubelet.qubelet.event.InboundPacketEventBus
import io.qubelet.qubelet.prot.GamePackets
import io.qubelet.qubelet.prot.inbound.handshake.HandshakeHandlers
import io.qubelet.qubelet.prot.inbound.login.LoginHandlers
import io.qubelet.qubelet.prot.inbound.play.PlayHandlers
import io.qubelet.qubelet.prot.inbound.status.StatusHandlers
import org.slf4j.LoggerFactory

/**
 * This handles all of the connection start to finish
 *
 * @property connections This is a map matching all clients to their GameConnection instances, its needed for the extension property
 */
object ConnHandler {
    init {
        HandshakeHandlers
        StatusHandlers
        LoginHandlers
        PlayHandlers
    }

    val logger = LoggerFactory.getLogger(this::class.java)
    val connections: MutableMap<Client, GameConnection> = mutableMapOf()

    /**
     * This function is called on every connection
     *
     * The packet reading cycle goes as follows, the packet size is read, then it reads in the raw packet based on the size
     * then matches the packet with its decoder and decodes it, before finally sending it to the event bus for handling.
     *
     * @param client This is the client that is connecting
     */
    fun handleConnection(client: Client) {
        connections[client] = GameConnection()
        connections[client]!!.client = client

        client.readVarIntAlways { size ->
            client.read(size) { rawPacket ->
                val opcode = rawPacket.varInt
                val packet = GamePackets.incoming[client.connection.state]!![opcode].decode(rawPacket, client.connection)

                InboundPacketEventBus.handlePre(packet)
                InboundPacketEventBus.handlePost(packet)
                logger.debug(packet.toString())
            }
        }

        client.preDisconnect {
            connections.remove(client)
        }
    }
}