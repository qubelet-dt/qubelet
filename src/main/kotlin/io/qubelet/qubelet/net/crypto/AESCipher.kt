package io.qubelet.qubelet.net.crypto

import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class AESCipher(val secretKey: ByteArray) {
    val algorithm = "AES/CFB8/NoPadding"
    val encryption: Cipher = Cipher.getInstance(algorithm)
    val decryption: Cipher = Cipher.getInstance(algorithm)

    init {
        val keySpec = SecretKeySpec(secretKey, "AES")
        val parameterSpec = IvParameterSpec(secretKey)
        encryption.init(Cipher.ENCRYPT_MODE, keySpec, parameterSpec)
        decryption.init(Cipher.DECRYPT_MODE, keySpec, parameterSpec)
    }

}