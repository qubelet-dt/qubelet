package io.qubelet.qubelet.net.crypto


import java.security.KeyPair
import java.security.KeyPairGenerator
import javax.crypto.Cipher


object RSAKeyPair {
    val algorithm = "RSA/ECB/PKCS1Padding"
    val encryption: Cipher = Cipher.getInstance(algorithm)
    val decryption: Cipher = Cipher.getInstance(algorithm)
    val keyPair: KeyPair

    init {
        val keyGen = KeyPairGenerator.getInstance("RSA")
        keyGen.initialize(1024)
        keyPair = keyGen.generateKeyPair()

        val privateKey = keyPair.private
        encryption.init(Cipher.ENCRYPT_MODE, privateKey)
        decryption.init(Cipher.DECRYPT_MODE, privateKey)
    }
}