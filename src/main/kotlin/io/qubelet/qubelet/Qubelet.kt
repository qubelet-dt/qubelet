package io.qubelet.qubelet

import com.github.simplenet.Server
import io.qubelet.qubelet.net.ConnHandler
import org.slf4j.LoggerFactory

/**
 * The main Qubelet class
 *
 * @constructor Binds server to port 25565
 * @property VERSION The version of MC the server supports
 * @property PROTOCOL The protocol version the server supports
 * @property MAX_PLAYERS The maximum amount of players
 * @property ONLINE_PLAYERS The amount of online players, hardcoded for now, needs changing
 * @property DESCRIPTION The server description
 */
object Qubelet : Server() {
    val logger = LoggerFactory.getLogger(this::class.java)

    // All of this needs to be automated
    val VERSION = "1.15.2"
    val PROTOCOL = 578
    val MAX_PLAYERS = 25
    val ONLINE_PLAYERS = 1
    val DESCRIPTION = "Hello, World!"

    init {
        onConnect(ConnHandler::handleConnection)
        logger.info("Server is online")
        bind("localhost", 25565)
    }
}

fun main() {
    Qubelet
}
