package io.qubelet.qubelet.entity

open class Entity {
    val id = count++

    companion object {
        var count = 0
    }
}