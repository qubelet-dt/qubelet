package io.qubelet.qubelet.entity

import io.qubelet.qubelet.prot.datatypes.Dimension
import io.qubelet.qubelet.prot.datatypes.Gamemode

class Player : Entity() {
    var gamemode = Gamemode.SURVIVAL
    var dimension = Dimension.OVERWORLD
    var viewDistance = 8
    lateinit var username: String
}